# autor :Seth Karim Luis Martinez
# email :guepardo190889@gmail.com
# date  :25/Mayo/2013
# file  :sendFBMessage.rb  
# description: this class send a message to inbox facebook with ruby 

require 'xmpp4r_facebook'
require 'fb_graph'
require '/usr/pbx-ruby/jarvix/others.rb'

class SendFBMessage

	attr_accessor:senderUserId
	attr_accessor:receiverUserId
	attr_accessor:receiverUserName
	attr_accessor:bodyMessage
	attr_accessor:senderUserToken

	#senderUserId = id del usuario que envia el mensaje
	#senderUserToke = Token del usuario que envia el mensaje
	#receiverUserName = Nombre del usuario que recibira el mensaje
	#bodyMessage = Cuerpo del mensaje que se envia
	#Note: The senderUserToken expires with the time, if you have problems, get a new tokenId
	def initialize(senderUserId, senderUserToken, receiverUserName, bodyMessage)  
    	@senderUserId = senderUserId
    	@receiverUserName = receiverUserName
    	@bodyMessage = bodyMessage
    	@senderUserToken = senderUserToken  
    	@receiverUserId = findReceiverUserId

    	puts "senderUserId: #{@senderUserId}"
    	puts "receiverUserName: #{@receiverUserName}"
    	puts "bodyMessage: #{@bodyMessage}"
    	puts "senderUserToken: #{@senderUserToken}"
		puts "receiverUserId: #{@receiverUserId}"
  	end 

  	#return -400 - No se encontro el 'receiverUserId'
  	#return -300 - Se encontraron mas de un users con el mismo 'receiverUserName'
  	#return >0 - Se encontro y se devuelve el 'receiverUserId'
  	def findReceiverUserId
  		isFoundReceiverUserId = false
  		senderUserIdCount = 0
  		receiverUserId = -1

  		user = FbGraph::User.me("#{@senderUserToken}")

		user.friends.each do |u|
			#puts "Friend:  - #{u.name}"
			userName = u.name
			if userName.include? @receiverUserName
				a = u.raw_attributes
			    receiverUserId = a[:id]
			    isFoundReceiverUserId = true
			    senderUserIdCount = senderUserIdCount + 1
			    #puts "receiverUserIdFounded: #{receiverUserId}"
			    #break
			end
		end

		#puts "isFound: #{isFound}"
		#puts "senderUserIdCount: #{senderUserIdCount}"
		#puts "Friends: #{user.friends.size}"

		if isFoundReceiverUserId
			if senderUserIdCount == 1
				return receiverUserId
			else
				return -300
			end
		else
			return -400
		end
  	end

	#return -400 - No se encontro ningun 'user'
	#return -300 - Se encontraron mas de un 'user'
	#return 'receiverUserId' Si el envio fue exitoso
  	def sendMessage
  		if @receiverUserId.to_i > 0
  			#puts "senderUserId: #{@senderUserId}"
  			#puts "receiverUserId: #{@receiverUserId}"

	  		#puts "sending_message"
			from = "-#{@senderUserId}@chat.facebook.com"
			to = "-#{@receiverUserId}@chat.facebook.com"
			body = @bodyMessage
			#subject = 'Message from Aura_1'
			message = Jabber::Message.new to, body
			#message.subject = subject

			#puts "from: #{from}"
			#puts "to: #{to}"
			#puts "body: #{body}"

			#puts "connecting"

			client = Jabber::Client.new Jabber::JID.new(from)
			client.connect
			#CAAGYdd7p6ZCcBAK3omNss2dhvQ6er4KldaLUexi75YY6sXBheZABsjLZAIYuIdZCKK29Xna7qcBVwRNXgCCzN7LNkxfi63z226Tx8G9dTCb9MuFS8RZCSA84ieU2ZBkVhFEDuZCgtDZCEtZBZBS9OPiZBsZBbClP5sC4HHkZD
			client.auth_sasl(Jabber::SASL::XFacebookPlatform.new(client, '449106995178487', 'CAAGYdd7p6ZCcBAGNpX2JwbwTqtZCd9ahKda7hgKqge1YphAt1P5024DI3KxAMNFIUGZCU9wBb7MLhhtvRo3psHrjrz6GxUrDPTK1yc7DcgZByZA4evcxOMZA7eHI89pzkshvU9Ni4ExQ4WdHXgAPLB9F1giYy5dvMZD', 'c80285a935ddf727e571ace59830d75b'), nil)
			client.send message
			#client.response_value
			client.close
			#puts 'message_sent_successfully'

			#Postear en el muero y postear nota
			stuff = Stuff.new(@senderUserId, @senderUserToken, @bodyMessage)
			stuff.wallPost
			stuff.postNote
			
			#return "MESSAGE_SENT_SUCCESSFULLY_TO:#{@receiverUserId}"
			return @receiverUserId
  		else
  			#return "ERROR_SENDING_MESSAGE:#{@receiverUserId}"
  			return @receiverUserId
  		end

  	end
end
