PROJECT_HOME = "/usr/pbx-ruby/"
require 'rubygems'
require 'net/http'		#for http connections
require 'yaml'
require 'resolv'
require 'active_record'
require '/usr/pbx-ruby/app/models/jarvix_salute.rb'
require '/usr/pbx-ruby/app/models/jarvix_word.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Abstract_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Object.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Custom_Property.rb'
require '/usr/pbx-ruby/app/models/JarvixPersonality.rb'
require '/usr/pbx-ruby/app/models/Jarvix_Conversation_Log.rb'
require '/usr/pbx-ruby/jarvix/JarvixPerson.rb'
require 'open-uri'
require 'json'
require 'uuid'
require 'uuidtools'
require '/usr/pbx-ruby/jarvix/ILineAccess.rb'
#require '/usr/pbx-ruby/jarvix/create_note.rb'

##########Rules for recognizing a conversation#############
#openPort
#opening a port requires speaking one word, to let the person who must receive the message jarvix is trying to establish a conversation
#
#
#openTransmission
#opening a transmission requires a question in which the person involved in the conversation must react opening the port (openPort) and
#responding to the openTransmission by answering the question.
#Sometimes the second party uses two openPort sequences (openPort)(openPort) which is equal to an (openTransmission) and there is no need to
#answer to the question jarvix initially did.
#sometimes the transmission is a happy face.
#
#
#payAttention
#Silence and recording sequence where the conversation is interpreted, is has to be mainly made in english, because of the strict grammar formation

class Jarvix
	
	#Trully used
	attr_accessor:openPortSalutes
	attr_accessor:openTransmissionSalutes
	attr_accessor:conversationStep
	attr_accessor:lineBreakers
	attr_accessor:brokenLines
	attr_accessor:lastMessage
	attr_accessor:myself
	attr_accessor:shellLetters
	attr_accessor:punctuations
	attr_accessor:wordsDefineQuestion
	attr_accessor:currentSubject
	attr_accessor:availablePersonalPronouns
	attr_accessor:availablePosessivePronouns
	attr_accessor:peopleInConversation
	attr_accessor:currentConversationLog
	attr_accessor:currentConversationID
	attr_accessor:textToAPI
	attr_accessor:machineToHumanResults
	attr_accessor:currentMainSubjectOfSentence
	attr_accessor:lineExecutionResult
	attr_accessor:activeApp
	attr_accessor:textForThirdPartyApp
	attr_accessor:userLang
	attr_accessor:personalitiesInLine
	attr_accessor:currentConversationID
	attr_accessor:linePointer
	attr_accessor:intentionsParameters
	attr_accessor:expectingKindOfParam
	attr_accessor:accessLine
	#Still unused
	
	attr_accessor:priorLastMessage

	#
	
	

	#
	def initialize (phoneNumber,lang)
		puts "init jarvix"
		# db_config = YAML::load( File.open("#{PROJECT_HOME}/config/database.yml"))
		# ActiveRecord::Base.establish_connection( db_config["JarvixBrain"])
		puts phoneNumber
		
		@currentMainSubjectOfSentence=nil
		@userLang=lang
		@currentConversationLog
		@openPortSalutes=nil
		@lineBreakers=nil
		@punctuations=nil
		@brokenLines=nil
		@textForThirdPartyApp=[]
		@activeApp=nil
		@textToAPI=[]
		@machineToHumanResults=[]
		@currentConversationLog=Jarvix_Conversation_Log.new
		@availablePosessivePronouns=nil
		@wordsDefineQuestion=nil
		@availablePersonalPronouns=nil
		@lastMessage=[]
		@shellLetters=""
		@expectingKindOfParam=nil
		#deactivate logger on DEBUG
		ActiveRecord::Base.logger.level = 1
		@myself=JarvixPerson.new("Jarvix")
		@currentSubject=JarvixPerson.new(phoneNumber)
		@accessLine=ILineAccess.new(@currentSubject.ontosData.id)
		#lets put the people in the conversation
		@peopleInConversation=[@myself,@currentSubject]
		@currentConversationID=UUIDTools::UUID.random_create.to_s()
		puts currentConversationID
		@currentConversationLog.chat_id=currentConversationID
		@currentConversationLog.user_id=@currentSubject.idPersonality
		puts "----END OF JARVIX----"
	end
	def conversationLogRecord
		@currentConversationLog=Jarvix_Conversation_Log.new
		@currentConversationLog.chat_id=@currentConversationID
		@currentConversationLog.user_id=@currentSubject.idPersonality
	end
	def openPort
		@conversationStep=0
		

		if @openPortSalutes==nil
			@openPortSalutes=Jarvix_Word.find_all_by_conversation_step(0)		 	
		end
		#At this point we select one of the openPorts Salutes to open up the social engineering operation
		return randomizeRegistry(@openPortSalutes)

	end
	def openTransmission
		
		fechaHoy=Time.now
		if fechaHoy.day == @currentSubject.ontosData.last_login.day
			#ya ingreso hoy, asi que poner mensaje corto
			@conversationStep=2
		else
			@conversationStep=1

		end
		@currentSubject.ontosData.last_login=Time.now.to_s(:db)
		@currentSubject.ontosData.save
		#Once we reach this point the script chooses saying an openPort command (word) or selecting a trully
		#openTransmission command, remember that two openPort operations made simultaneasly is the same as opening the transmission
		decideDoubleOpenPort=rand(0..10)
		#if(decideDoubleOpenPort>7)
			#30% of chances to get a doubleOpenPort
			#return openPort
		#else
			#Until this point jarvix has spoken and introduced himself, it is now time for the user to write/talk
		@openTransmissionSalutes=Jarvix_Word.find_all_by_conversation_step(@conversationStep)
		
		return randomizeRegistry(@openTransmissionSalutes)
		#end
		
		
	end

	def payAttention(messageFromConversation)
		#look for the ENTER

		# if messageFromConversation.index(/[\n\r]+/)==nil
		# 	#pegamos la(s) letras
		# 	@shellLetters=@shellLetters+messageFromConversation
		# 	return
		# end
		messageFromConversation=wordExpanders(messageFromConversation)
		if @shellLetters==""
			messageFromConversation="# "+removePunctuation(messageFromConversation).downcase.strip+" #"	
		end
		if @shellLetters!=""
			messageFromConversation="# "+removePunctuation(String.new(@shellLetters)).downcase.strip+" #"
		end
		
		@shellLetters=""
		conversationLogRecord()
		@currentConversationLog.original_line=messageFromConversation
		#Analyze the message in thid_e next steps
		#QUESTION?
		#ANSWER?
		#COMMAND

		#The first three words will tell us what are any of the prior cases with one example
		#for each one:
		#QUESTION: Are(1) you(2) going(3)				Auxiliar	+Personal Noun	+verb				= YES/NO
		#QUESTION: Where(1) is(2) dominos(3) pizza		question 	+Auxiliar		+_____				= PLACE/LOCATION
		#QUESTION: When(1) do(2) you(3) leave			question 	+Auxiliar 		+Personal Noun		= DATE
		#QUESTION: Can(1) you(2) work(3) tomorrow		Auxiliar 	+Personal Noun	+verb				= YES/NO

		#WORD_TYPE(0)=TO BE
		#WORD_TYPE(1)=PERSONAL_NOUN
		#WORD_TYPE(2)=QUESTION_WORD
		#WORD_TYPE(3)=VERB
		#WORD_TYPE(4)=AUXILIAR ex. DO, DID,HAVE,CAN
		#WORD_TYPE(5)=ADJECTIVE/NOUN
		#WORD_TYPE(6)=AFFIRMATIVE
		#WORD_TYPE(7)=NEGATIVE
		#WORD_TYPE(8)=PUNCTUATION
		#WORD_TYPE(9)=SALUTES
		#WORD_TYPE(10)=EXPANDERS
		#WORD_TYPE(11)=POSESSIONS
		#WORD_TYPE(12)=COLOR
		#WORD_TYPE(13)=OBJECT
		#WORD_TYPE(14)=QUANTIFIERS
		#WORD_TYPE(15)=LOCATION
		#WORD_TYPE(16)=NAME
		#WORD_TYPE(17)=SENTENCE_HELPER
		#WORD_TYPE(18)=RELATIVES
		#WORD_TYPE(19)=GENERAL_INFO
		#WORD_TYPE(20)=BRANDS
		#WORD_TYPE(21)=TIME_PERIODS
		#WORD_TYPE(22)=WORD_POINTERS
		#WORD_TYPE(23)=NUMBERS

		#__pointer__ apunta un sobrenombre a una persona por ejemplo "mi papa"
		#__equals__ iguala palabras a otras palabras o personas a palabras ejemplo "mi papa __equals__ maestro" o "gato __equals__ mamifero"
		#__relate__ relaciona personas con otras personas

		#>>>>>>>HOW=Raw data, list, array, invoke
		#>>>>>>>are=tipo status, . , from
		#>>>>>>>YOU=THIS

		#how much money do you want
		#raw-tipo number,count,length-money-aux-THIS-ask,pending answer

		#how did you do your homework
		#raw-aux-THIS-DO-.-:property=>homework

		#how many children do you have
		#raw-tipo number,count,length-children-aux-THIS-totalIndexOf

		#how to drive
		#raw 

		#how old are you
		#raw-StartDate-.-THIS

		#what is your name
		#search-.-THIS-name

		#what is a teacher
		#search-.-1-teacher

		#what are you thinking
		#search-.-THIS-thinking

		#who is my brother
		#ID-.-THIS-brother

		#where are you
		#location-.-THIS

		#where is my car?
		#position-.-MY-car


		#ANSWER: Yes I am home
		#ANSWER: it is near the river
		#ANSWER: tomorrow morning
		#ANSWER: yes I can

		#possible possitive answers
		#YES=GOOD=OK=PERFECT=FINE >>PROCEED(true)
		#Possible negative answers
		#NO=NOT='NT'=
		#In other words, asking "how are you?" is like asking for the state of a var if(myvar){}
		#which answer obviously reflects positiveness or badness

		##########CODE###########find the several instructions in the sentence
		if @lineBreakers==nil
			@lineBreakers=Jarvix_Word.where("is_question like '%1%' or is_question like '%2%' or is_sentence like '%1%' or is_sentence like '%2%'")

		end
		@brokenLines=[]
		splitedMessage=messageFromConversation.split(" ")
		replaceMessage=String.new(messageFromConversation)
		@lineBreakers.each do |lineBreaker|
			indice=messageFromConversation.index(" "+lineBreaker.word.downcase+" ")
			if indice!=nil
				replaceMessage=replaceMessage.gsub(" "+lineBreaker.word.downcase+" "," ^ ")
			end
		end

		# puts messageFromConversation
		 puts replaceMessage
		splitedMessageRecognize=replaceMessage.split(" ")
		#divide sentences
		startCut=0
		saveFirstWord=nil
		currentMessageLine=""
		for indexWordInConversation in (0..splitedMessageRecognize.length)
			#ver si la primer palabra es question word, en caso de que sea guardar la primera palabra
			if indexWordInConversation==1
				#puts "buscando:"
				#puts splitedMessage[indexWordInConversation]
				saveFirstWord=Jarvix_Word.find_by_word(splitedMessage[indexWordInConversation])
				if saveFirstWord!=nil
					#puts "found first word"
					
					if saveFirstWord.word_type!=2
						saveFirstWord=nil
					end
						
					
				end
			end
			#HOT FIX no puede haber más de 3 seguidos, de modo que si tenemos 4 linebrakers debemos eliminar no solo el siguiente sino los dos siguientes
			if splitedMessageRecognize[indexWordInConversation]=="^" && splitedMessageRecognize[indexWordInConversation+1]=="^" && splitedMessageRecognize[indexWordInConversation+2]=="^" && splitedMessageRecognize[indexWordInConversation+3]=="^"
				splitedMessageRecognize[indexWordInConversation+1]="_"
				splitedMessageRecognize[indexWordInConversation+2]="_"
				sentenceToInsert=removePunctuation(currentMessageLine)
				puts "BRA 1"
				if sentenceToInsert!="" && sentenceToInsert!=" "
					@brokenLines.push(sentenceToInsert)
				end
				currentMessageLine=""
				saveFirstWord=nil
			end
			if splitedMessageRecognize[indexWordInConversation]=="^" && splitedMessageRecognize[indexWordInConversation+1]=="^" && saveFirstWord==nil && splitedMessageRecognize[indexWordInConversation+2]!="#"
				# puts "BR2_S"
				#this makes disappear the second haystack so we just choose the first one
				splitedMessageRecognize[indexWordInConversation+1]="_"
				sentenceToInsert=removePunctuation(currentMessageLine)
				puts sentenceToInsert
				if sentenceToInsert!="" && sentenceToInsert!=" "
					@brokenLines.push(sentenceToInsert)
				end
				currentMessageLine=""
				# puts "BR2_E"
			end
			
			if splitedMessage[indexWordInConversation]!=nil
				# puts "BR3_S"
				
				currentMessageLine=currentMessageLine+" "+splitedMessage[indexWordInConversation]
				# puts currentMessageLine
				# puts "BR3_E"

			end
		end
		#pushes final line
		@brokenLines.push(removePunctuation(currentMessageLine))
		#if the next word is pat of the pattern, then we are talking various sentences
		puts @brokenLines
		@currentConversationLog.broken_lines=@brokenLines.split("\n")
		#before diving sentences, move messages
		@priorLastMessage=@lastMessage
		@lastMessage=Array.new(@brokenLines)
		@linePointer=nil
		
		#once the sentence is broken, we need to analyze each sentence and determine which are questions and
		#which are trash
		@personalitiesInLine=[]
		@lineExecutionResult=nil
		@lastMessage.each do |lineInMessage|
			if @linePointer==nil
				@linePointer=lineInMessage.split(" ")
			end
			puts @lastMessage.inspect
			@currentMainSubjectOfSentence=nil
			if @activeApp==nil
				humanToMachine(removePunctuation(lineInMessage))
			else
				#si tenemos el "i am done" cerramos la app

				if removePunctuation(lineInMessage).index("i am done")!=nil
					puts "CLOSING APP"
					#antes de cerrar la aplicacion crear un objeto nuevo y grabar
					#note = Create_Note.new('Note of '+@currentSubject.name("GETTER",""),@textForThirdPartyApp.join(" "))
					note.new_note

					@textForThirdPartyApp=[]
					@activeApp=nil
					next
				end
				#guardamos todo el texto
				@textForThirdPartyApp.push(lineInMessage)
				puts "EXECUTING APP:"+@activeApp
			end
		end
		@currentConversationLog.text_to_api=@textToAPI.split("\n")
		@currentConversationLog.save
		@textToAPI.each do |command|
			puts "EXE>"+command.strip
			if command.index("NOT_POSESSIVE_IN_SENTENCE")!=nil
				@lineExecutionResult="Can you redo the question?"
			else
				if @accessLine.waitingParam!=nil
					#esta parte se ejecuta cuando
					@lineExecutionResult=newIntention("SETTER",@linePointer.join(","),"NONE")
					break
				else
					@lineExecutionResult = eval command
				end
			end
			#si recibimos una respuesta como APP_ON:NOMBRE_DE_APP
			#todo lo que venga después se va a esa app hasta que digamos "i am done"
			# if @lineExecutionResult!=nil
			# 	if @lineExecutionResult.index("APP_ON")!=nil
			# 		#hay aplicacion activa
			# 		@activeApp=@lineExecutionResult[7,@lineExecutionResult.length-7]
			# 		puts "aplicacion activa es:"+@activeApp
			# 	end
			# 	puts @lineExecutionResult
			# end
		end
		@textToAPI=[]
		puts "____SOR_____"
		puts @lineExecutionResult.inspect
		puts "____EOR_____"
		if @lineExecutionResult.nil?
			@lineExecutionResult="OK"
		end
		return @lineExecutionResult
	end
	def reply
		
	end
	def closePort
		
	end
	def closeTransmission
		
	end

	def addSpacer(noSpacerString, commandSeparator)
		return noSpacerString+commandSeparator+" "
	end
	def humanToMachine(singleLineMessage)
		puts "-------"
		#used
		@intentionsParameters=[]
		currentQuestionWordForLine=nil
		singleWordProcessing=singleLineMessage.split(" ")
		currentExecutionTask=isQuestion(singleLineMessage)
		puts currentExecutionTask
		executionMethodQueue=[]
		objectsOfConversation=[]
		propertiesOfConversation=[]
		currentExecutionEval=""
		#splits the message analyzing the first two words
		
		if isNegative(singleLineMessage)==true
			#puts singleLineMessage.strip+" is NEGATIVE"	
			#currentExecutionTask="!"+currentExecutionTask
		end
		#metodo para sustituir las posesiones de gente por su correspondiente pronombre personal
		currentSentencePronoun=getPersonalPronoun(singleLineMessage)[0]
		finalExecutionMethod="NONE"
		finalExecutionMethodRaw=""
		finalExecutionMethodRawSecond=[]
		numberOfMethods=0
		lastWordType=0
		for singleWordProcessingIndex in (0..singleWordProcessing.length)
			#search word
			#verificar si la palabra esta en plural
			singleWordDB=nil
			
			isPlural=isWordPlural(singleWordProcessing[singleWordProcessingIndex])
			if isPlural!=nil
				singleWordDB=Jarvix_Word.find_by_word(isPlural)
			else
				singleWordDB=Jarvix_Word.find_by_word(singleWordProcessing[singleWordProcessingIndex])
			end
			if singleWordDB!=nil
				

				#identificar metodos
				#identificamos si es question word
				if singleWordDB.word_type==16
					#encontramos un nombre, asi que hay que crear la personalidad y sustituirla por
					#el probombre que le coresponda, en caso que encontremos en la linea
					#la palabra para nombre, entonces no debemos cargar la personalidad sino tratarlo solo como propiedad
					
					if singleWordProcessing.index("name")!=nil
						puts "INQ_SET_N"
						propertiesOfConversation.push(singleWordDB)
					else
						puts "INQ_N"
						justNewPerson=addPersonToConversation(singleWordDB,singleWordProcessing,singleWordProcessingIndex)
						# puts justNewPerson.inspect
						@personalitiesInLine.push(justNewPerson)
						# puts "+++++"
						#splitedMessage[indexWordInConversation]=justNewPerson.thirdPersonalPronoun
						singleWordProcessing[singleWordProcessingIndex]=justNewPerson.thirdPersonalPronoun
						@linePointer[singleWordProcessingIndex]=justNewPerson.ontosData.id
						puts @linePointer.inspect
						#se agrega la oracion modificada con la personalidad ya cargada a la conversacion
						@lastMessage.push(singleWordProcessing.join(" "))
						return
					end
				end
				if singleWordDB.word_type==2 && singleWordDB.result_is.index("Invoke")==nil && singleWordDB.result_is.index("Ignore")==nil
					puts "QW:"+singleWordDB.word
					currentQuestionWordForLine=singleWordDB
				end
				if singleWordDB.word_type==13 || singleWordDB.word_type==18
					#this is an object or a relative
					
					if lastWordType==3 #si es un verbo, necesitamos el complemento
						puts "INQ5.1:"+singleWordDB.word
						propertiesOfConversation.push(singleWordDB)
					else
						puts "INQ5.2:"+singleWordDB.word
						#frases como "my phone number is 999" incluyen dos objetos seguidos, lo cual quiere decir
						#que el primero es el objeto mientras que el segundo es la propiedad, por lo que antes de agregarlo a la lista de objetos
						#hay que comprobar si la palabra anterior es objeto y si no forma parte
						#si la palabra anterior no es verbo, el complemento no es necesario y se agrega en la fila como corresponde
						if lastWordType==13 and singleWordDB.word_type==13
							puts "INQ5.21:"+singleWordDB.word
							#fix para cuando es pregunta "what is your phone number" se retira el status
							if executionMethodQueue.length>0
								if executionMethodQueue[0].word_type==0
									executionMethodQueue.delete_at(0)
								end
							end
							if singleLineMessage.index(" and ")!=nil
								puts "INQ5.22:"+singleWordDB.word
								#buscar -and- y ver si esta antes que esta palabra o despues
								#si esta antes es objeto, si esta después o no existe es propiedad
								if singleWordProcessing.index(singleWordDB.word)>singleWordProcessing.index("and")
									#es objeto
									puts "INQ5.23:"+singleWordDB.word
									objectsOfConversation.push(singleWordDB)
								else
									#es propiedad
									puts "INQ5.24:"+singleWordDB.word
									executionMethodQueue.push(singleWordDB)
								end
								
							else
								#no tiene and, es probable que sea una propiedad
								executionMethodQueue.push(singleWordDB)
							end
							

						else
							#agregamos el objeto y le damos next para que no se guarde en otras partes como propiedades de los intentions
							puts "INQ5.25:"+singleWordDB.word
							objectsOfConversation.push(singleWordDB)
							#se agregaron objetos a la conversacion, es mas probable que haya que eliminar el -status- de los metodos a ejecutar
							armonyPosessionStatus(executionMethodQueue,singleWordDB)
							#aqui tenemos un detalle por que, necesitamos convertir los "my car" en sujetos y agregarlos a la lista de objetos que se hablan
							#también otros sujetos como "my dad" son sujetos con personalidad
						end
					end
					if singleWordDB.word_type==18
						puts "TENEMOS FAMILIAR:"+singleWordDB.word
						currentPosessiveForLineDum=getPersonalPronounByPosession(singleLineMessage)
						newPersonalityFromChat=nil
						if currentPosessiveForLineDum!=nil
							puts "currentPosessiveForLineDum:"+currentPosessiveForLineDum.word
							#se busca en base al posesivo cual es la persona ya cargada que es contacto del usuario
							customPointerPersonalPronoun=Jarvix_Word.find_by_word(currentPosessiveForLineDum.word)
							puts "DEBUG"
							customPointerPersonalPronounString=getResultFromPosition(customPointerPersonalPronoun.result_is)
							puts customPointerPersonalPronounString
							#ya teniendo el pronombre personal podemos intercambiarlo por el objeto de persona
							customPersonObject=getID(customPointerPersonalPronounString)
							#@personalitiesInLine.push(customPersonObject)
							# puts customPersonObject.inspect
							#una referencia personal se usa en la base de datos usando la conotación __pointer__ y en valor se usa el id de la personalidad
							#en caso de no encontrar ningun apuntador a la personalidad se crea una, cuando se vaya a crear la referencia real de la personalidad, se debe
							#crear un nuevo __pointer__, esto permite saber cuando el usario entro antes o después
							createNewObject(singleWordDB.word)
							#buscar el abstract object
							newAbstractPersonalReference=Jarvix_Abstract_Object.find_by_object_name(singleWordDB.word)
							newCustomPersonalReference=Jarvix_Custom_Object.where('id_abstract_object='+newAbstractPersonalReference.id.to_s+" and id_owner="+@currentSubject.ontosData.id.to_s).first
							customPointer=Jarvix_Custom_Property.where("id_user="+customPersonObject.idPersonality.to_s()+" and property_name='__pointer__' and id_custom_object="+newCustomPersonalReference.id.to_s)
							if customPointer.length==0
								puts "NINGUNA REFERENCIA SE DEBE CREAR UNA REFERENCIA customPointerPersonalPronoun:"+customPointerPersonalPronoun.word+"  customPointerPersonalPronounString:"+customPointerPersonalPronounString+"   singleWordDB:"+singleWordDB.word
								newPersonalityFromChat=JarvixPerson.new(1234567890)
								# puts newPersonalityFromChat.inspect
								newCustomPropertyReference=Jarvix_Custom_Property.new(:id_custom_object=>newCustomPersonalReference.id,:id_user=>@currentSubject.ontosData.id,:property_name=>"__pointer__",:property_value=>newPersonalityFromChat.ontosData.id,:is_enabled=>1)
								newCustomPropertyReference.save
								@peopleInConversation.push(newPersonalityFromChat)
								newPersonalityFromChat.isThird=true
								@personalitiesInLine.push(newPersonalityFromChat)
							else
								puts "HAY REFERENCIA: "+customPointer[0].property_value.to_s
								#se encontro referencia a un objeto de personalidad, entonces hay que cargarlo y traerlo a la conversación
								newPersonalityFromChat=JarvixPerson.new(customPointer[0].property_value.to_i())
								newPersonalityFromChat.isThird=true
								@peopleInConversation.push(newPersonalityFromChat)
								@personalitiesInLine.push(newPersonalityFromChat)
							end
						end
						#rompemos el metodo para no avanzar jj
						
						#buscamos el indice donde encontramos el familiar, y copiamos el resto de la oración
						puts "imprimiendo"
						copiaImper=nil
						if currentExecutionTask=="GETTER"
							puts "REF1:"+singleWordProcessingIndex.to_s
							#si es pregunta copiarlos al reves
							# copiaImper=singleWordProcessing[0,singleWordProcessingIndex]
							singleWordProcessing[singleWordProcessingIndex]=newPersonalityFromChat.thirdPersonalPronoun
							@linePointer[singleWordProcessingIndex]=newPersonalityFromChat.ontosData.id
							copiaImper=singleWordProcessing
							#verificar si la palabra anterior es posesiva
							puts "REF1.1 "
							if singleWordProcessing[singleWordProcessingIndex-1]==currentPosessiveForLineDum.word
								#palabra anterior es posesiva entonces cortar

								#copiaImper=singleWordProcessing[0,singleWordProcessingIndex-1]
								puts singleWordProcessing.join("-")
								
								singleWordProcessing[singleWordProcessingIndex-1]=newPersonalityFromChat.thirdPersonalPronoun
								@linePointer[singleWordProcessingIndex-1]=newPersonalityFromChat.ontosData.id
								@linePointer.delete_at(singleWordProcessingIndex-1)
								puts @linePointer.inspect
								singleWordProcessing.delete_at(singleWordProcessingIndex-1)
								copiaImper=singleWordProcessing
								
								puts singleWordProcessing.join("-")
							end
						else
							puts "REF2_S:"+singleWordProcessingIndex.to_s
							#si es una oracion normal seguimos como al principio
							#copiaImper=singleWordProcessing[singleWordProcessingIndex+1,singleWordProcessing.length-singleWordProcessingIndex]
							if newPersonalityFromChat!=nil
								singleWordProcessing[singleWordProcessingIndex]=newPersonalityFromChat.thirdPersonalPronoun	
							end
							
							puts copiaImper.inspect
							@linePointer[singleWordProcessingIndex]=newPersonalityFromChat.ontosData.id
							copiaImper=singleWordProcessing
							if singleWordProcessing[singleWordProcessingIndex-1]==currentPosessiveForLineDum.word
								#palabra anterior es posesiva entonces cortar

								#copiaImper=singleWordProcessing[0,singleWordProcessingIndex-1]
								puts singleWordProcessing.join("-")
								
								singleWordProcessing[singleWordProcessingIndex-1]=newPersonalityFromChat.thirdPersonalPronoun
								@linePointer[singleWordProcessingIndex-1]=newPersonalityFromChat.ontosData.id
								@linePointer.delete_at(singleWordProcessingIndex-1)
								puts @linePointer.inspect
								singleWordProcessing.delete_at(singleWordProcessingIndex-1)
								copiaImper=singleWordProcessing
								
								puts singleWordProcessing.join("-")
							end
							puts "REF2_E"
							# @linePointer=@linePointer[singleWordProcessingIndex+1,singleWordProcessing.length-singleWordProcessingIndex]
						end
						listaDeObjetosString=""
						objectsOfConversation.each do |singleParticularObject|
							if singleParticularObject.word_type!=18
								listaDeObjetosString=listaDeObjetosString+" "+singleParticularObject.word
							end
						end
						ultimaPosesionDeTercero=convertPersonalPronounToPosessive(newPersonalityFromChat.currentPersonalPronoun)
						#verificar si se estan igualando las personalidades
						#en caso de que las personalidades se igualen, hay que apuntarlas entre si
						if isMergingPeople(singleWordProcessing.join(" "))==true
							puts "MERGE"
							# puts @personalitiesInLine.inspect
							mergePeople(@personalitiesInLine)
							return "OK"
						else
							if currentExecutionTask=="GETTER"
								puts copiaImper.join(" ")+" INQ11"
								@lastMessage.push(copiaImper.join(" "))
							else
								puts copiaImper.join(" ")+" INQ10"
								@lastMessage.push(copiaImper.join(" "))
								#reescribimos la oracion de las personalidades
								@linePointer=@linePointer.join(" ")
								@linePointer=@linePointer.split(" ")
								puts @linePointer.inspect
							end
						end
						return
						#buscamos a esta persona
					end
				end
				if  singleWordDB.word_type==15 && singleWordDB.is_method=="0"
					#buscar los tipos 15 que sean metodos con esto decidimos si es objeto o lugar
					isUsed=false
					current15Index=singleLineMessage.index(singleWordDB.word)
					all15AsMethod=Jarvix_Word.find_all_by_word_type(15)
					all15AsMethod.each do |singleEntry|
						
						if singleEntry!=nil
							
							if singleEntry.is_method=="1"
								
								#puts current15Index 
								
								currentSingleEntryPosition=singleLineMessage.index(" "+singleEntry.word+" ")
								puts currentSingleEntryPosition
								if currentSingleEntryPosition!=nil
									#puts "->"+singleEntry.word.strip+"->"+currentSingleEntryPosition.to_s()
									if current15Index>currentSingleEntryPosition
										puts "INQ7:"+singleWordDB.word
										#si es un location si esta despues de at, in
										propertiesOfConversation.push(singleWordDB)
										isUsed=true
									end
								end
							end
						end
					end
					if isUsed==false
						objectsOfConversation.push(singleWordDB)
					end
				end
                if singleWordDB.word_type==1 and singleWordDB.word.downcase!="i" and singleWordDB.word.downcase!="you"
                   #tenemos un pronombre asi que buscar cual es y agregarlo a la lista
                    puts "INQ1_12:"
                    justNewPerson=addPersonToConversation(singleWordDB,singleWordProcessing,singleWordProcessingIndex)
					@personalitiesInLine.push(justNewPerson)
				elsif executionMethodQueue.length==0 && (singleWordDB.word_type==3 || singleWordDB.word_type==0 || singleWordDB.word_type==5 || (singleWordDB.word_type==15 && singleWordDB.is_method=="1") )
					puts "INQ1_1:"+singleWordDB.word
					#if singleWordDB.word_type==0 && currentQuestionWordForLine==nil
					executionMethodQueue.push(singleWordDB)
					armonyPosessionStatus(executionMethodQueue,singleWordDB)
					
					#end
				elsif executionMethodQueue.length==1
					puts "INQ2.1:"+singleWordDB.word
					 if executionMethodQueue[0].word_type==0 && (singleWordDB.word_type==3 || singleWordDB.word_type==5 || (singleWordDB.word_type==15 && singleWordDB.is_method=="1")) || singleWordDB.word_type==12
						#remplaza el verbo to be, por el nuevo
						puts "INQ2:"+singleWordDB.word
						executionMethodQueue[0]=singleWordDB;
					elsif singleWordDB.result_is.index("Invoke")==nil && singleWordDB.result_is.index("Ignore")==nil && singleWordDB.word_type!=0 && singleWordDB.word_type!=3 && singleWordDB.word_type!=5 && singleWordDB.word_type!=15  && singleWordDB.word_type!=2 && singleWordDB.word_type!=1 && singleWordDB.word_type!=13 && singleWordDB.word_type!=11 && singleWordDB.word_type!=16
						puts "INQ4:"+singleWordDB.word
						propertiesOfConversation.push(singleWordDB)
					# elsif singleWordDB.word_type==16
					# 	#crear objeto persona y cargarlo
					# 	#en caso de que ya este cargado dejarlo ahi
					# 	puts "INQ4.1:"+singleWordDB.word
					# 	justNewPerson=addPersonToConversation(singleWordDB,singleWordProcessing,singleWordProcessingIndex)
					# 	@personalitiesInLine.push(justNewPerson)
					# 	return
						#normalmente aqui dejamos todo y reescribimos la linea con el pronombre personal
					else
						puts "INQ2.11:"+singleWordDB.word
						#guardamos todas las palabras restantes que no sean las helpers del idioma
						if singleWordDB.word_type!=17 and singleWordDB.word_type!=1 and singleWordDB.word_type!=14 and singleWordDB.word_type!=15 and isWordInArrays(singleWordDB.word,objectsOfConversation)==false
							puts "INQ2.12:"+singleWordDB.word
							@intentionsParameters.push(singleWordDB.word)	
						end
					end
				elsif (singleWordDB.word_type==3 || singleWordDB.word_type==5 || (singleWordDB.word_type==15 && singleWordDB.is_method=="1"))
					puts "INQ3:"+singleWordDB.word
					executionMethodQueue.push(singleWordDB)
				elsif singleWordDB.result_is.index("Invoke")==nil && singleWordDB.result_is.index("Ignore")==nil && singleWordDB.word_type!=0 && singleWordDB.word_type!=3 && singleWordDB.word_type!=5 && singleWordDB.word_type!=15  && singleWordDB.word_type!=2 && singleWordDB.word_type!=1  && singleWordDB.word_type!=13 && singleWordDB.word_type!=11 && singleWordDB.word_type!=18
					puts "INQ6:"+singleWordDB.word
					if singleWordDB.word_type==16
						#es un nombre, entonces crear la personalidad o buscarla
						#buscar el nombre y el apellido en 
						justNewPerson=addPersonToConversation(singleWordDB,singleWordProcessing,singleWordProcessingIndex)
						@personalitiesInLine.push(justNewPerson)
						@lastMessage.push(singleWordProcessing.join(" "))
						@linePointer[singleWordProcessingIndex]=justNewPerson.ontosData.id
						puts @linePointer.inspect
						return
					else
						propertiesOfConversation.push(singleWordDB)
					end
				end
				lastWordType=singleWordDB.word_type
			else
				if singleWordProcessing[singleWordProcessingIndex]!=nil
					puts "no existe: "+singleWordProcessing[singleWordProcessingIndex]
					#antes de crear este objeto compararlo y verificar si es un numero
					#puede ser un numero telefonico o alguna cuenta, o correo electronico
					
					if is_numeric(singleWordProcessing[singleWordProcessingIndex]) or validate_email_domain(singleWordProcessing[singleWordProcessingIndex])==true
						puts "ES NUMERO? "	
						#entonces agregarlo a la lista de propiedades
						newNumber=Jarvix_Word.new
						newNumber.word=singleWordProcessing[singleWordProcessingIndex]
						newNumber.word_type=23
						newNumber.is_question=0
						newNumber.is_sentence="X"
						newNumber.result_is="X:"+singleWordProcessing[singleWordProcessingIndex]
						propertiesOfConversation.push(newNumber)
					else
						puts "NO ES NUMERO"
					#crea objeto y regresa su word object
						justCreated=createNewObject(singleWordProcessing[singleWordProcessingIndex])
						objectsOfConversation.push(justCreated)
					end
					
				end
			end
		end
		if currentExecutionTask=="GETTER"
			#reverse
			finalExecutionMethodRawSecond=finalExecutionMethodRawSecond.reverse
		end
		#puts "RAW:"+finalExecutionMethodRawSecond.join(".")
		#en este punto ya tenemos el verbo, pero tiene más peso un posesivo que un question word, comprobamos si tenemos posesivo
        @personalitiesInLine=makeUniqueValuesInArray(@personalitiesInLine)
		currentPosessiveForLine=getPersonalPronounByPosession(singleLineMessage)
		#si encontramos posesivo empezar a formar el executionQueu
		if currentPosessiveForLine!=nil
			puts "posessiveLine:"+currentPosessiveForLine.word
			cleanStatusAndGetID(propertiesOfConversation)
			@currentMainSubjectOfSentence=getSubjectOfFirstObject(singleLineMessage,objectsOfConversation,propertiesOfConversation)
			#whosePosession(currentLineExecution,objectsOfConversation)
			#currentExecutionEval='getID("'+getResultFromPosition(currentPosessiveForLine.result_is)+'").posessions("'+currentExecutionTask+'","'
			currentExecutionEval='getID("'+@currentMainSubjectOfSentence+'")'
			if executionMethodQueue[0]!=nil
				if Jarvix.method_defined?(executionMethodQueue[0].word)==true or @accessLine.waitingParam!=nil
					currentExecutionEval="self."
				elsif JarvixPerson.method_defined?(executionMethodQueue[0].word)==true
					currentExecutionEval+="."+executionMethodQueue[0].word
				elsif Jarvix_Custom_Object.method_defined?(executionMethodQueue[0].word)==true
					puts "_error_"
				else
					currentExecutionEval+=".posessions"
				end
			else
				currentExecutionEval+=".posessions"
			end
			currentExecutionEval+='("'+currentExecutionTask+'","'
			currentExecutionEval+=buildExec(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,nil)
			currentExecutionEval+='")'
		elsif currentQuestionWordForLine!=nil
			#se antepone primero
			puts "questionWordLine"
			cleanStatusAndGetID(propertiesOfConversation)
			currentExecutionEval=subject(singleLineMessage)+'.'+getResultFromPosition(currentQuestionWordForLine.result_is)+'("'+currentExecutionTask+'","'
			
			currentExecutionEval+=buildExec(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,nil)

			currentExecutionEval+='")'
		else
			executionMethodQueue.each do |elemento|
				#buscamos el metodo en la conversacion, si lo encontramos entonces dejamos que nos provea de exec string
				#si no lo encontramos entonces debe estar en JarvixPerson o JarvixObjec
				methodToLookUp=getResultFromPosition(elemento.result_is)
				puts "methodToLookUp:"+methodToLookUp
				#depues de que se asimilaron las palabras, hay que probar si lineAccess se esta ejecutando
				if Jarvix.method_defined?(methodToLookUp)==true or @accessLine.waitingParam!=nil
					#borrar el metodo
					puts "JxONE"
					executionMethodQueue.delete(elemento)
					# currentExecutionEval=subject(singleLineMessage)+"."+methodToLookUp+'("'+currentExecutionTask+'","'
					currentExecutionEval="self."+methodToLookUp+'("'+currentExecutionTask+'","'
				elsif JarvixPerson.method_defined?(methodToLookUp)==true
					puts "Jx2_"
					executionMethodQueue.delete(elemento)
					currentExecutionEval=subject(singleLineMessage)+"."
					if methodToLookUp=="status" and objectsOfConversation.length>0
						currentExecutionEval=currentExecutionEval+"posessions"
					else
						currentExecutionEval=currentExecutionEval+methodToLookUp
					end
					currentExecutionEval=currentExecutionEval+'("'+currentExecutionTask+'","'
				elsif Jarvix_Custom_Object.method_defined?(methodToLookUp)==true
					puts "Jx3"
					executionMethodQueue.delete(elemento)
					currentExecutionEval=subject(singleLineMessage)+"."
					if methodToLookUp=="status" and objectsOfConversation.length>0
						currentExecutionEval=currentExecutionEval+"posessions"
					else
						currentExecutionEval=currentExecutionEval+methodToLookUp
					end
					currentExecutionEval=currentExecutionEval+'("'+currentExecutionTask+'","'
				else
					#seguramente es una descripcion
					puts "NOT_METHOD:"+elemento.word+" objects:"+objectsOfConversation.length.to_s
					if objectsOfConversation.length>0
						currentExecutionEval=subject(singleLineMessage)+'.posessions("'+currentExecutionTask+'","'
					else
						currentExecutionEval=subject(singleLineMessage)+'.description("'+currentExecutionTask+'","'
					end
					
				end
				buildExecString=buildExec(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,methodToLookUp)
				if buildExecString=="" 
					buildExecString=@intentionsParameters.join(",")
				end
				currentExecutionEval+=buildExecString
				if methodToLookUp=="newIntention"
					#agregar parametro adicional de personas
					currentExecutionEval+='","'+extractIdFromPerson(@personalitiesInLine)
				end
				currentExecutionEval+='")'
				#currentExecutionEval=elemento.word.strip+'()'
				if @accessLine.waitingParam!=nil
					break
				end
			end
		end
		@textToAPI.push(currentExecutionEval.strip)
		#

		#if it was a question then it is a GETTER if it was a sentence it was a SETTER
		if currentSentencePronoun=="NOT_FOUND"
			#
			puts "not executed due to lack of pronoun"
			#myjson=JSON.parse(open("http://api.nl.cic.mx/0/nl/reports.json?limit=2&for_group=136").read)
			#puts "reading json"
			#puts myjson["reports"][0]["content"]
		
		end
	end

	def getSubjectOfFirstObject(currentLineExecution,objectsOfConversation,propertiesOfConversation)
		#puts "gSOFO_start"
		if objectsOfConversation!=nil
			if objectsOfConversation.length>0
		#		puts "gSOFO_inside"
				objectWord=objectsOfConversation[0].word
				return whosePosession(currentLineExecution,objectWord)
			end
			if propertiesOfConversation.length>0
		#		puts "gSOFO_inside"
				objectWord=propertiesOfConversation[0].word
				return whosePosession(currentLineExecution,objectWord)
			end
		end
		if propertiesOfConversation.length>0
		#		puts "gSOFO_inside"
			objectWord=propertiesOfConversation[0].word
			return whosePosession(currentLineExecution,objectWord)
		end
		#if we reached this point then return the first finding of the posession
		return whosePosession(currentLineExecution,nil,true)
		#puts "gSOFO_end"
		return "NOT_FOUND"
	end
	def buildExec(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,methodToLookUp)
		currentExecutionEval=""
		puts "buildExec1: "
		#probar contra las intenciones
		if methodToLookUp!="newIntention"
			for objetoIndex in (0..objectsOfConversation.length)
				puts "buildExec2: "
				if objectsOfConversation[objetoIndex]!=nil
					puts "buildExec3: "+objectsOfConversation[objetoIndex].word
					currentExecutionEval+=objectsOfConversation[objetoIndex].word+">"
					objectsOfConversation[objetoIndex]=nil
					if currentQuestionWordForLine!=nil
						currentExecutionEval+=getResultFromPosition(currentQuestionWordForLine.result_is)
					
					end
					puts "buildExec3.1"
					currentExecutionEval=buildExecHelper(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,currentExecutionEval)
					
					 if objetoIndex<objectsOfConversation.length-1
					 	currentExecutionEval+=","
					 end
				end
			end
			if currentExecutionEval[-1,1]==">"
				currentExecutionEval=currentExecutionEval[0,currentExecutionEval.length-1]
			end
			if objectsOfConversation.length==0
				puts "buildExec3.2"
				#no tiene ningún objeto en la conversacion y por lo tanto puede causar errores, entonces
				#mejor ponerlo como description
				currentExecutionEval=buildExecHelper(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,currentExecutionEval)
			end
			#puts currentExecutionEval
			if currentExecutionEval=="status"
				#es un residuo sin procesamiento
				currentExecutionEval==""
			end
		else
			puts "buildExec3.3"
			#cuando es un newIntention, los verbos van antes que los objetos, esto nos permite buscarlos
			currentExecutionEval=extractWordFromObject(objectsOfConversation)+"."+@intentionsParameters.join(";")
			puts currentExecutionEval
			
		end
		return currentExecutionEval
	end
	def buildExecHelper(objectsOfConversation,executionMethodQueue,propertiesOfConversation,currentQuestionWordForLine,singleLineMessage,currentExecutionEval)
		for elementoIndex in (0..executionMethodQueue.length)
			#buscamos el metodo en la conversacion, si lo encontramos entonces dejamos que nos provea de exec string
			#si no lo encontramos entonces debe estar en JarvixPerson o JarvixObject
			puts "executionMethodQueue2-1:"+elementoIndex.to_s
			if executionMethodQueue[elementoIndex]!=nil
				methodToLookUp=getResultFromPosition(executionMethodQueue[elementoIndex].result_is)
				if (methodToLookUp=="status" && elementoIndex==0 && currentQuestionWordForLine==nil)
					
					#checar si hay un metodo anterior
					if  (methodToLookUp!="status" and methodToLookUp!="posessions")
						puts "executionMethodQueue2:"+methodToLookUp
						if currentExecutionEval[currentExecutionEval.length-1]=="|"
							currentExecutionEval[currentExecutionEval.length-1]=">"
						end
						currentExecutionEval+=methodToLookUp+"|"
					end
				end
			end
		end
		for propertyIndex in (0..propertiesOfConversation.length)
			if propertiesOfConversation[propertyIndex]!=nil
				#if propertyIndex==0
				#	currentExecutionEval+="|"
				#end
				if (propertiesOfConversation[propertyIndex]=="status" && propertyIndex==0 && currentQuestionWordForLine==nil) || propertiesOfConversation[propertyIndex]!="status"
					puts "propertiesOfConversation2:"+getResultFromPosition(propertiesOfConversation[propertyIndex].result_is)
					currentExecutionEval+=getResultFromPosition(propertiesOfConversation[propertyIndex].result_is)
					whP=whosePosession(singleLineMessage,propertiesOfConversation[propertyIndex].word)
					if whP!="NOT_POSESSIVE_IN_SENTENCE"
						currentExecutionEval+="."+whP
					end
				end
				# if propertyIndex==(propertiesOfConversation.length-1)
				# 	currentExecutionEval+=","
				# end
			end
		end
		if currentExecutionEval[-1,1]=="|"
			currentExecutionEval=currentExecutionEval[0,currentExecutionEval.length-1]
		end
		return currentExecutionEval			
	end
	def whosePosession(currentLineExecution,currentWordPosessed,firstFinding=false)
		#puts "WP_start"
		#puts currentLineExecution
		#puts currentWordPosessed
		currentLineExecution="# "+currentLineExecution+" #"

		currentPositionIndex=-1
		if currentWordPosessed!=nil
			currentPositionIndex=currentLineExecution.index(currentWordPosessed)
		end
		currentPosessiveSearch=nil
		lastPosition=-1
		if currentPositionIndex!=nil
			@availablePosessivePronouns.each do |ppronoun|
				
				i = -1
				while i = currentLineExecution.index(" "+ppronoun.word+" ",i+1)
					if firstFinding==true
						currentPosessiveSearch=ppronoun
						return getResultFromPosition(currentPosessiveSearch.result_is)
						
					end
					if i<currentPositionIndex && lastPosition<i
						lastPosition=i
						#puts "FOUND:"+ppronoun.word
						#puts currentWordPosessed
						currentPosessiveSearch=ppronoun
					end
				end
			end
		end
		if currentPosessiveSearch==nil
			puts "NOT_POSESSIVE_IN_SENTENCE"
			return "NOT_POSESSIVE_IN_SENTENCE"
		end
		#puts "result_is"
		#puts currentPosessiveSearch.word
		if getResultFromPosition(currentPosessiveSearch.result_is)!=@currentMainSubjectOfSentence
			return getResultFromPosition(currentPosessiveSearch.result_is)
		else
			return "NOT_POSESSIVE_IN_SENTENCE"
		end
	end
	def createNewObject(objectName)
		newAbstract=nil
		newObjectRelated=nil
		newWord=nil
		#no existe aun el objeto, asi que lo creamos
		#verificar que de vdd no exista
		newWord=Jarvix_Word.find_by_word(objectName)
		if newWord==nil
			newWord=Jarvix_Word.new(:word=>objectName,:is_question=>0,:is_sentence=>"X",:result_is=>"X:"+objectName,:word_type=>13,:is_breakline=>0,:is_method=>0)
			newWord.save	
		end
		newAbstract=Jarvix_Abstract_Object.find_by_object_name(objectName)
		if newAbstract==nil
			#se creo el registro nuevo, ahora a crear el objeto abstracto con la propiedad y setear la propiedad customizada en el usuario
			newAbstract=Jarvix_Abstract_Object.new(:object_name=>objectName)
			newAbstract.save	
		end
		
		#relacionarlo con el usuario
		#to do, no siempre se habla de uno mismo, adaptar el id_owner
		newObjectRelated=Jarvix_Custom_Object.new(:id_abstract_object=>newAbstract.id,:id_owner=>@currentSubject.idPersonality)
		newObjectRelated.save
		return newWord
		
	end
	
	def isNegative(sentenceString)
		return sentenceString.index("not")!=nil
	end
	def isQuestion(sentenceString)
		if @wordsDefineQuestion==nil
			@wordsDefineQuestion=Jarvix_Word.where("is_question>0")
		end
		positionsCorrected=0
		splitedSentenceString=sentenceString.split(' ')
		@wordsDefineQuestion.each do |currentQuestionPosition|
			testCase=splitedSentenceString.index(String.new(currentQuestionPosition.word).downcase)
			if testCase!=nil
				#parece que lo encontro, ahora veamos si efectivamente es para la pregunta
				testCase=testCase+1;
				if String.new(currentQuestionPosition.is_question).index(testCase.to_s())!=nil
					positionsCorrected=positionsCorrected+1
				end
			end
		end
		if positionsCorrected>1
			return "GETTER"
		end
		return "SETTER"
	end

	def randomizeRegistry(registryToRandom)
		
		offset=rand(registryToRandom.size)
		if registryToRandom.size>0
			registryToRandom[offset].translate
		end
		if userLang=="en"
			if @conversationStep==0
				return addSpacer(@openPortSalutes[offset]['word'],'')
			end
			return addSpacer(registryToRandom[offset]['word'],'')
		else
			if @conversationStep==0
				return addSpacer(@openPortSalutes[offset][userLang+'_word'],'')
			end
			return addSpacer(registryToRandom[offset][userLang+'_word'],'')
		end
	end

	def removePunctuation(stringToAnalize)
		stringCorrected=String.new(stringToAnalize)
		if @punctuations==nil
			@punctuations=Jarvix_Word.find_all_by_word_type(8)
		end
		#analize string and remove punctuation
		@punctuations.each do |punctuation|
			if stringCorrected.index(punctuation.word)!=nil
				stringCorrected=stringCorrected.gsub(punctuation.word,"")
			end
		end
		return stringCorrected.strip
	end
	def wordExpanders(lineToExpand)
		#this method expands the contracted words such as isnt,arent,im,aint,isn't etc
		lineExpanded="# "+String.new(lineToExpand).downcase+" #"
		expanders=Jarvix_Word.find_all_by_word_type(10)
		expanders.each do |currentExpander|
			if lineExpanded.index(" "+currentExpander.word.strip+" ")!=nil
				lineExpanded=lineExpanded.gsub(" "+currentExpander.word.strip+" "," "+currentExpander.result_is.strip+" ")
			end
		end
		return removePunctuation(lineExpanded).strip
	end
	def getID(personalPronoun,personalityID=-2)
		#puts "GETTING IDENTIFICATION OBJECT FOR:"+personalPronoun.strip
		#we first look for the person object
		puts "IN_GETID"
		if personalityID<0 and personalityID!=-2
			personalityID=0
		end
		if personalPronoun=="I"
			personalityID=-2
		end
		realPersonalityId=@linePointer.join(" ")[(personalityID+1)..@linePointer.join(" ").length]
		#en lugar de recortar dos, recortamos hasta encontrar espacio
		spaceIndex=nil
        if realPersonalityId!=nil
            realPersonalityId.index(" ")
        end
		if spaceIndex==nil
			realPersonalityId=realPersonalityId.to_i
		else
			realPersonalityId=realPersonalityId[0,spaceIndex].to_i
		end

		personInMessage=nil
		@peopleInConversation.each do |currentSearchPerson|
			puts "if ("+currentSearchPerson.currentPersonalPronoun+ "=="+personalPronoun+" and ("+currentSearchPerson.ontosData.id.to_s+"=="+realPersonalityId.to_s+" or -2=="+personalityID.to_s+")) or ("+currentSearchPerson.isThird.to_s+" and "+currentSearchPerson.thirdPersonalPronoun+"=="+personalPronoun+" and ("+currentSearchPerson.ontosData.id.to_s+"=="+realPersonalityId.to_s+" or personalityID=="+personalityID.to_s+"))"
			if (currentSearchPerson.currentPersonalPronoun==personalPronoun and (currentSearchPerson.ontosData.id==realPersonalityId or personalityID==-2)) or (currentSearchPerson.isThird==true and currentSearchPerson.thirdPersonalPronoun==personalPronoun and (currentSearchPerson.ontosData.id==realPersonalityId or personalityID==-2))
				puts "found_person"
				personInMessage=currentSearchPerson
				break
			end
		end
		#si no encontro un usuario y es -you- es probable que preguntaran a jarvix2,3
		if personalPronoun.downcase=="you" and personInMessage==nil
			personInMessage=@myself
		end
		return personInMessage
	end
	def getPersonalPronoun(currentLineString)
		personalPronounReturn=nil
		rewriteCurrentLineString="# "+currentLineString+" #"
		rewriteCurrentLineStringWithNumbers="# "+@linePointer.join(" ")+" #"
		if @availablePersonalPronouns==nil
			#this line searches only personal pronouns
			@availablePersonalPronouns=Jarvix_Word.find_all_by_word_type(1)
			
		end
		foundOnIndex=0
		@availablePersonalPronouns.each do |singlePronoun|
			if rewriteCurrentLineString.index(" "+singlePronoun.word.downcase+" ")!=nil
				foundOnIndex=rewriteCurrentLineString.index(" "+singlePronoun.word.downcase+" ")-2
				personalPronounReturn=singlePronoun
				break;
			end
		end
		return [personalPronounReturn,foundOnIndex]
	end
	#este metodo convierte un pronombre personal en su pronombre posesivo
	def convertPersonalPronounToPosessive(pronombre)
		personalPronoun=Jarvix_Word.where("word_type='11' and result_is like 'X:#{pronombre}'")
		return personalPronoun[0].word
	end
	def getPersonalPronounByPosession(currentLineString)
		personalPronounReturn=nil
		rewriteCurrentLineString="# "+currentLineString+" #"
		if @availablePosessivePronouns==nil
			@availablePosessivePronouns=Jarvix_Word.find_all_by_word_type(11)
		end
		@availablePosessivePronouns.each do |singlePronoun|
				if rewriteCurrentLineString.index(" "+singlePronoun.word.downcase+" ")!=nil
					#personalPronounReturn=String.new(singlePronoun.result_is)[2,singlePronoun.result_is.length-2]
					personalPronounReturn=singlePronoun
					break;
				end
		end
		return personalPronounReturn
	end
	def isWordPlural(currentWord)
		#hay dos reglas para el plural, si la palabra termina en s o en es
		if currentWord==nil
			return currentWord
		end
		#puts "isWordPlural"
		#puts currentWord[0,currentWord.length-2]
		#puts currentWord[0,currentWord.length-1]
		returnSingularWord=nil
		if currentWord[-2,2]=="es"
			returnSingularWord=Jarvix_Word.find_by_word(currentWord[0,currentWord.length-2])
			if returnSingularWord!=nil
				return returnSingularWord.word
			end
		elsif currentWord[-1,1]=="s" and currentWord.length>3
			#hay que verificar que sea mayor que 3 letras, por que hay palabras que terminan en S pero son de tres y esta regla no aplica
			#ejemplo HIS
			puts "TRYING TO ERASE:"+currentWord
			returnSingularWord=Jarvix_Word.find_by_word(currentWord[0,currentWord.length-1])
			if returnSingularWord!=nil
				return returnSingularWord.word
			end
		end
		return currentWord
	end
	def pluralToSingular
		
	end
	def areWeTalkingAboutSomethingOrSomeone(lineString)
		#buscamos y verificamos si hablamos de algo o de alguien y lo agregamos en la conversacion
	end
	def searchVerbWithIng(lineString)
		#regresa el verbo si es que existe sin ING, si no encuentra regresa nil
	end

	#this method makes sure that when we look for some Jarvix_Word object, it cleans the -result_is- field
	def getResultFromPosition(lineResult,currentIndexPosition=-2)
		separatedResults=lineResult.split(",")
		separatedResults.each do |elemento|
			separatedInIndex=elemento.split(":")
			if separatedInIndex[0]=="X" && currentIndexPosition==-2
				return separatedInIndex[1]
			elsif Integer(separatedInIndex[0])==currentIndexPosition
				return separatedInIndex[1]
			end
		end
	end
	def subject(currentLineString)
		currentPersonalPronoun=getPersonalPronoun(currentLineString)
		if currentPersonalPronoun==nil
			currentReturn='self'	
		else
			currentReturn='getID("'+currentPersonalPronoun[0].word+'",'+currentPersonalPronoun[1].to_s+')'
		end
		return currentReturn
	end
	def cleanStatusAndGetID(arrayToClean)
		arrayClon=Array.new(arrayToClean)
		
		
		return arrayClon
	end
	def jarvixSystem(requestType,systemAction)
		puts "executing system"
		return self
	end
	def location(requestType,systemAction)
		puts "SEARCHING LOCATION"
	end

	#metodo para cargar personalidades desde nombres
	def addPersonToConversation(singleWordDB,singleWordProcessing,singleWordProcessingIndex)
		#primero ver si ya tenemos esa persona agregada
		@peopleInConversation.each do |eachPersonByName|
			if eachPersonByName.isThird==true
				#analizar la tercera persona
				usnombre=eachPersonByName.name("GETTER","")
				if usnombre.downcase.index(singleWordDB.word.downcase)!=nil or eachPersonByName.thirdPersonalPronoun==singleWordDB.word.downcase
					#salir del loop por que ya esta cargado
					#pero sobreescribir el nombre por el pronombre
					puts "QUIT ADD"
					singleWordProcessing[singleWordProcessingIndex]=eachPersonByName.thirdPersonalPronoun
					#buscar
					puts singleWordProcessing.join("-")
					return eachPersonByName
				end
			end
		end
		# personalityFromNameOne=Jarvix_Personality.where("my_name like '%"+singleWordDB.word+"%'")
		# puts "INQ6.1 name"
		# puts personalityFromNameOne.inspect
		# if personalityFromNameOne.length==0
		# 	#no existe personalidad para el nombre
			
		# end
		#ahora buscar de los anteriores quienes estan relacionados
		personalityFromRelate=Jarvix_Custom_Property.where(:property_name=>"__pointer__",:id_user=>@currentSubject.ontosData.id,:id_custom_object=>nil)
		#si tenemos personas iterarlas sino, crear la personalidad
		if personalityFromRelate.length>0
			puts "INQ6.11 "
			#iterar las personas conocidas por el nombre dado
			personalidades=[]
			personalityFromRelate.each do |eachPersonality|
				#hay que buscar las personalidades que son iguales a los apuntadores y si lo son cargar esas personalidades
				personalityEquals=Jarvix_Custom_Property.where(:property_name=>"__equals__",:id_user=>eachPersonality.property_value,:id_custom_object=>nil)
				if personalityEquals.length>0
					#se encontraron personalidades igualadas ahora hay que ver si es el nombre
					personalityEquals.each do |singlePersonalityEquals|
						
						personalityEqualsFromNameOne=Jarvix_Personality.where("my_name like '%"+singleWordDB.word.downcase.downcase+"%' and id="+singlePersonalityEquals.property_value)
						if personalityEqualsFromNameOne.length>0
							puts "EQUALS Jarvix_Personality=>"+"my_name like '%"+singleWordDB.word.downcase+"%' and id="+singlePersonalityEquals.property_value
							personalidades.push(singlePersonalityEquals)
						end
					end
						
				end
				puts "Jarvix_Personality=>"+"my_name like '%"+singleWordDB.word+"%' and id="+eachPersonality.property_value
				personalityFromNameOne=Jarvix_Personality.where("my_name like '%"+singleWordDB.word+"%' and id="+eachPersonality.property_value)
				if personalityFromNameOne.length>0
					personalidades.push(eachPersonality)	
				end
			end
			if personalidades.length>0
				puts "INQ6.12 "
				# puts personalidades.inspect
				# puts personalidades[0].id
				#si hay personalidades, cargar la personalidad
				loadPersonalityFromName=JarvixPerson.new(personalidades[0].property_value)
				loadPersonalityFromName.isThird=true
				loadPersonalityFromName.name("SETTER",singleWordDB.word)
				@peopleInConversation.push(loadPersonalityFromName)
				singleWordProcessing[singleWordProcessingIndex]=loadPersonalityFromName.thirdPersonalPronoun
				#buscar
				puts singleWordProcessing.join("-")
				#se comenta la linea para aislar el metodo y solo cargue nombres o sobrenombres
				# @lastMessage.push(singleWordProcessing.join(" "))
				return loadPersonalityFromName
			else
				puts "INQ6.13 "
				#no se encontro ninguna personalidad al nombre
				#entonces crear una y relacionarlas entre si
				createPersonalityFromName=JarvixPerson.new(1234567890)
				createPersonalityFromName.isThird=true
				createPersonalityFromName.name("SETTER",singleWordDB.word)
				@peopleInConversation.push(createPersonalityFromName)
				newPersonRelation=Jarvix_Custom_Property.new(:property_name=>"__pointer__",:property_value=>@currentSubject.ontosData.id,:id_user=>createPersonalityFromName.ontosData.id)
				newPersonRelation.save
				newPersonRelationSource=Jarvix_Custom_Property.new(:property_name=>"__pointer__",:property_value=>createPersonalityFromName.ontosData.id,:id_user=>@currentSubject.ontosData.id)
				newPersonRelationSource.save
				@peopleInConversation.push(createPersonalityFromName)
				return createPersonalityFromName
			end
		else
			puts "INQ6.14 "
			#crear la personalidad de la que se habla
			createPersonalityFromName=JarvixPerson.new(1234567890)
			createPersonalityFromName.isThird=true
			createPersonalityFromName.name("SETTER",singleWordDB.word)
			@peopleInConversation.push(createPersonalityFromName)
			newPersonRelation=Jarvix_Custom_Property.new(:property_name=>"__pointer__",:property_value=>@currentSubject.ontosData.id,:id_user=>createPersonalityFromName.ontosData.id)
			newPersonRelation.save
			newPersonRelationSource=Jarvix_Custom_Property.new(:property_name=>"__pointer__",:property_value=>createPersonalityFromName.ontosData.id,:id_user=>@currentSubject.ontosData.id)
			newPersonRelationSource.save
			@peopleInConversation.push(createPersonalityFromName)
			return createPersonalityFromName
		end
		puts "INQ6.2 name"
		# puts personalityFromRelate.inspect
		return @peopleInConversation[@peopleInConversation.length-1]
	end

	def addNickNameToConversation(singleWordDB)
		currentPosessiveForLineDum=getPersonalPronounByPosession(singleLineMessage)
		newPersonalityFromChat=nil
		if currentPosessiveForLineDum!=nil
			puts "currentPosessiveForLineDum:"+currentPosessiveForLineDum.word
			#se busca en base al posesivo cual es la persona ya cargada que es contacto del usuario
			customPointerPersonalPronoun=Jarvix_Word.find_by_word(currentPosessiveForLineDum.word)
			puts "DEBUG"
			customPointerPersonalPronounString=getResultFromPosition(customPointerPersonalPronoun.result_is)
			
			#ya teniendo el pronombre personal podemos intercambiarlo por el objeto de persona
			customPersonObject=getID(customPointerPersonalPronounString)
			puts customPersonObject.inspect
			#una referencia personal se usa en la base de datos usando la conotación __pointer__ y en valor se usa el id de la personalidad
			#en caso de no encontrar ningun apuntador a la personalidad se crea una, cuando se vaya a crear la referencia real de la personalidad, se debe
			#crear un nuevo __pointer__, esto permite saber cuando el usario entro antes o después
			createNewObject(singleWordDB.word)
			#buscar el abstract object
			newAbstractPersonalReference=Jarvix_Abstract_Object.find_by_object_name(singleWordDB.word)
			newCustomPersonalReference=Jarvix_Custom_Object.where('id_abstract_object='+newAbstractPersonalReference.id.to_s+" and id_owner="+@currentSubject.ontosData.id.to_s).first
			customPointer=Jarvix_Custom_Property.where("id_user="+customPersonObject.idPersonality.to_s()+" and property_name='__pointer__' and id_custom_object="+newCustomPersonalReference.id.to_s)
			if customPointer.length==0
				puts "NINGUNA REFERENCIA SE DEBE CREAR UNA REFERENCIA customPointerPersonalPronoun:"+customPointerPersonalPronoun.word+"  customPointerPersonalPronounString:"+customPointerPersonalPronounString+"   singleWordDB:"+singleWordDB.word
				newPersonalityFromChat=JarvixPerson.new(1234567890)
				newCustomPropertyReference=Jarvix_Custom_Property.new(:id_custom_object=>newCustomPersonalReference.id,:id_user=>@currentSubject.ontosData.id,:property_name=>"__pointer__",:property_value=>newPersonalityFromChat.ontosData.id,:is_enabled=>1)
				newCustomPropertyReference.save
				@peopleInConversation.push(newPersonalityFromChat)
				newPersonalityFromChat.isThird=true
			else
				puts "HAY REFERENCIA: "+customPointer[0].property_value.to_s
				#se encontro referencia a un objeto de personalidad, entonces hay que cargarlo y traerlo a la conversación
				newPersonalityFromChat=JarvixPerson.new(customPointer[0].property_value.to_i())
				newPersonalityFromChat.isThird=true
				@peopleInConversation.push(newPersonalityFromChat)
			end
		end
		#rompemos el metodo para no avanzar jj
		
		#buscamos el indice donde encontramos el familiar, y copiamos el resto de la oración
		puts "imprimiendo"
		copiaImper=nil
		if currentExecutionTask=="GETTER"
			puts "REF1:"+singleWordProcessingIndex.to_s
			#si es pregunta copiarlos al reves
			# copiaImper=singleWordProcessing[0,singleWordProcessingIndex]
			singleWordProcessing[singleWordProcessingIndex]=newPersonalityFromChat.thirdPersonalPronoun
			copiaImper=singleWordProcessing
			#verificar si la palabra anterior es posesiva
			puts "REF1.1 "
			if singleWordProcessing[singleWordProcessingIndex-1]==currentPosessiveForLineDum.word
				#palabra anterior es posesiva entonces cortar

				#copiaImper=singleWordProcessing[0,singleWordProcessingIndex-1]
				puts singleWordProcessing.join("-")
				
				singleWordProcessing[singleWordProcessingIndex-1]=newPersonalityFromChat.thirdPersonalPronoun
				singleWordProcessing.delete_at(singleWordProcessingIndex-1)
				copiaImper=singleWordProcessing

				puts singleWordProcessing.join("-")
			end
		else
			puts "REF2"
			#si es una oracion normal seguimos como al principio
			copiaImper=singleWordProcessing[singleWordProcessingIndex+1,singleWordProcessing.length-singleWordProcessingIndex]
		end
		listaDeObjetosString=""
		objectsOfConversation.each do |singleParticularObject|
			if singleParticularObject.word_type!=18
				listaDeObjetosString=listaDeObjetosString+" "+singleParticularObject.word
			end
		end
		ultimaPosesionDeTercero=convertPersonalPronounToPosessive(newPersonalityFromChat.currentPersonalPronoun)
		if currentExecutionTask=="GETTER"
			puts copiaImper.join(" ")+" INQ11"
			@lastMessage.push(copiaImper.join(" "))
		else
			puts newPersonalityFromChat.thirdPersonalPronoun+" "+listaDeObjetosString+" "+copiaImper.join(" ")+" INQ10"
			@lastMessage.push(newPersonalityFromChat.thirdPersonalPronoun+" "+listaDeObjetosString+" "+copiaImper.join(" "))
		end
	end

	#este metodo compara dos personas, es probable que sean personalidades diferentes
	#lo cual ayuda a determinar si son igualaciones
	def isMergingPeople(sentence)
		replacedSentence=sentence.gsub(" is ","'=='")
		replacedSentence=replacedSentence.gsub(" are ","'=='")
		replacedSentence="'"+replacedSentence+"'"
		puts replacedSentence
		return eval replacedSentence
	end

	def mergePeople(arrayPeople)
		if arrayPeople.length>0
			#guardar el primero y unirlo con el resto
			firstPerson=arrayPeople[0]

			arrayPeople.each do |singlePerson|
				#primero verificar que no sea el mismo id,
				if firstPerson.ontosData.id!=singlePerson.ontosData.id
					#verificar que no exista la union
					newCustomPropertyReferences=Jarvix_Custom_Property.where(:id_user=>firstPerson.ontosData.id,:property_name=>"__equals__",:property_value=>singlePerson.ontosData.id)
					if newCustomPropertyReferences.length==0
						#no existe una referencia de igualacion, crear
						newCustomPropertyReference=Jarvix_Custom_Property.new(:id_user=>firstPerson.ontosData.id,:property_name=>"__equals__",:property_value=>singlePerson.ontosData.id)
						newCustomPropertyReference.save
						newCustomPropertyReference=Jarvix_Custom_Property.new(:id_user=>singlePerson.ontosData.id,:property_name=>"__equals__",:property_value=>firstPerson.ontosData.id)
						newCustomPropertyReference.save
					end
				end
			end
		end
	end

	def is_numeric(obj) 
   		obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
	end

	def validate_email_domain(email)
      domain = email.match(/\@(.+)/)
      if domain!=nil
      	domain=domain[1]
      else
      	return false
      end
      Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
      end
      @mx.size > 0 ? true : false
	end
	#este metodo es para recibir todas las intenciones o los WANTS,
	def extractWordFromObject(words)
		stringPLine=""
		words.each do |singlePLine|
			stringPLine=stringPLine+singlePLine.word+";"
		end
		if stringPLine!="" and stringPLine!=nil
			return stringPLine[0,stringPLine.length-1]
		else
			return "NONE"
		end
	end

	def extractIdFromPerson(people)
		stringPLine=""
		people.each do |person|
			stringPLine=stringPLine+person.ontosData.id.to_s+";"
		end
		if stringPLine!="" and stringPLine!=nil
			return stringPLine[0,stringPLine.length-1]
		else
			return "NONE"
		end
	end

	def newIntention(requestType,allRequests,peopleInvolved)

			#aqui debemos tener una interfaz con las tablas de lineAccess para determinar las aplicaciones
			if @accessLine.waitingParam==nil
				puts "JX1.1"
				accessLineResponse=@accessLine.runApp(allRequests,peopleInvolved)
				#primero revisamos si espera un parametro
				if @accessLine.waitingParam!=nil
					return lineAccessParamMessage(@accessLine.waitingParam)
				end
				return accessLineResponse
			else
				if @accessLine.waitingParam=="-person"
					#buscar id de persona
					possiblePeople=allRequests.split(",")
					possiblePeople.each do |singlePosiblePeople|
						if is_numeric(singlePosiblePeople)==true
							#es numero, posiblemente es el id, suministrar parametro
							@accessLine.addNewParam(singlePosiblePeople.to_s)
							#ahora volver a ejecutar
							puts "TRYING:runApp('"+@accessLine.currentApp.name+"."+@accessLine.currentAppMethod.name+"','"+@accessLine.currentAppParams.join(",")+"')"
							responseLine=@accessLine.runApp(@accessLine.currentApp.name+"."+@accessLine.currentAppMethod.name,@accessLine.currentAppParams.join(","))
							return lineAccessParamMessage(responseLine)
						end
					end
				else
					@accessLine.addNewParam(@brokenLines.join(" "))
					puts "TRYING:runApp('"+@accessLine.currentApp.name+"."+@accessLine.currentAppMethod.name+"','"+@accessLine.currentAppParams.join(",")+"')"
					responseLine=@accessLine.runApp(@accessLine.currentApp.name+"."+@accessLine.currentAppMethod.name,@accessLine.currentAppParams.join(","))
					return lineAccessParamMessage(responseLine)
				end
			end
		return "No application could be executed"
	end
	def armonyPosessionStatus(seed,word)
		indice=-1
		indicep=-1
		for indexSeed in (0..seed.length)
			if seed[indexSeed]==nil
				next
			end
			puts seed[indexSeed].inspect
			if seed[indexSeed].result_is=="X:status" 
				indice=indexSeed
			end
		end
		if indice!=-1
			puts "armony1.1"
			#encontramos status, ahora ver si la palabra es status tmb
			if word.word_type==0
				#es status, asi que dejarlo en paz
			elsif word.word_type==13 or word.word_type==23
				#no se encontraron status, revisar si es un objeto
				#si es objeto reemplazar por posessiones
				seed[indice].result_is="X:posessions"
			end
		end
			
	end
    def makeUniqueValuesInArray(source)
       tmpArray=[]
        source.each do |eachsource|
            if tmpArray.index(eachsource)==nil
                tmpArray.push(eachsource)
            end
        end
        return tmpArray
    end
	def isWordInArrays(string,arrayOne)
		arrayOne.each do |singleOne|
			if singleOne.word==string
				return true
			end
		end
		return false
	end
	def lineAccessParamMessage(param)
		if param=="-person"
			return "please tell me the person"
		elsif param=="-text"
			return "please tell me the text"
		else
			return param
		end
			
	end
	
end