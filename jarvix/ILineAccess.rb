require '/usr/src/apiManagerCore/db/model/sys_user.rb'
require '/usr/src/apiManagerCore/db/model/api_method.rb'
require '/usr/src/apiManagerCore/db/model/api_interface.rb'
require '/usr/src/apiManagerCore/google/JMail.rb'
require '/usr/src/apiManagerCore/twitter/JTwitter.rb'
require '/usr/src/apiManagerCore/facebook/JFacebook.rb'


class ILineAccess

	attr_accessor:currentApp
	attr_accessor:currentAppParams
	attr_accessor:currentAppMethod
	#esta variable es nil cuando no espera parametros, es -person cuando espera el ID de una personality
	#es -text cuando espera cualquier otro texto
	attr_accessor:waitingParam
	attr_accessor:waitingConfirmation
	attr_accessor:currentUserID

	def initialize(userID)
		@currentUserID=userID
		@waitingParam=nil
		@waitingConfirmation=false
		@currentAppParams=[]
	end

	#USO
	#cuando recien queremos ejecutar una app
	#runApp('email.send','NONE')
	#
	#cuando hizo falta un parametro
	#addNewParam('hola k ace')
	#runApp(instancia.currentApp.name,instancia.currentAppParams)
	def runApp(appName,appParams)
		appMethod=appName.split(".")
		#verifica si existe esa app, por ejemplo facebook.chat
		#en caso que exista NONE, significa que no se especifico la app
		if appMethod[0]=="NONE"
			#buscar el metodo y dar los posible metodos expuestos
			puts "ILA1"
			metodos=findAppMethod(appMethod[1])
			if metodos.length>0
				puts "ILA1.1"
				#si existen metodos pero no tenemos cual de todas esas interfaces es la que desea ejecutar
				return "there are a couple of applications with that "+appMethod[1]+" you should tell me the application"
			end
		else
			#parece que tenemos app y metodo, buscar primero app
			findApp(appMethod[0])
			#
			
			if @currentApp!=nil
				
				#ahora buscar si el metodo lo tiene la app
				if appContainsMethod(appMethod[0],appMethod[1])!=nil
					
					#existe el metodo, ahora revisar el numero de parametros
					@currentAppParams=appParams.split(",")
					
					realAppParams=@currentAppMethod.params.split(",")
					
					if appParams=="NONE"
						@currentAppParams=[]
					end
					if @currentAppParams.length<realAppParams.length
						#faltan parametros, buscar nombre del parametro siguiente que falta
						@waitingParam=realAppParams[@currentAppParams.length]
						return realAppParams[@currentAppParams.length]
					else
						#quizas hay mas parametros pero solo tomamos los necesarios
						####aqui corre la app#####
						@waitingParam=nil
						puts "RUNNING APP!"
						#crear objeto
						mainAppEx=eval @currentApp.command
						if mainAppEx!=nil
							begin
								mainAppEx.fromIdUser=@currentUserID
								mainAppExResult=mainAppEx.send("_"+@currentAppMethod.name,*@currentAppParams)
								return mainAppExResult	
							rescue Exception => e
								return @currentApp.name+" says that "+e.message
							end
							
						end
						return "that application is not installed yet"
					end
				else
					return "sorry i cannot use that command; it is not installed yet"
				end
			else
				return "no application with that name was found"
				waitingParam=nil
			end

		end
	end

	def findApp(appName)
		#puts "FA1:"+appName
		@currentApp=Api_Interface.find_by_name(appName)
		#puts @currentApp.inspect
		return @currentApp
	end

	def findAppMethod(method)
		metodos=Api_Method.find_all_by_name(method)
		return metodos
	end
	def listPossibleApps(method)

	end
	def appContainsMethod(appName,method)
		findApp(appName)
		metodos=findAppMethod(method)
		metodos.each do |singleMethod|
			if singleMethod.api_interface==@currentApp.id
				@currentAppMethod=singleMethod
				return singleMethod
			end
		end
		return nil
	end

	def newIntent(appName,appParams)
		
	end

	def addNewParam(param)
		if @currentAppParams==nil
			@currentAppParams=[]
		end
		@currentAppParams.push(param)
	end

	def classFactoryBuilder(appName)

	end
end
