# autor :Héctor Acosta
# email :hacost@hotmail.com
# date  :25-may-2013
# file  :create_note.rb  
# description: this class create one note in evernote with ruby  

require "digest/md5"
require 'evernote-thrift'
require 'date'  # Needed for Date and DateTime
#require 'uuid'

class Create_Note 
   #attr_accessor:userStore
    #attr_accessor:authToken
    #attr_accessor:notes
	attr_accessor:d

  def initialize(title_note, body_note)  
    # Instance variables  
    #@user = user  
    @title_note = title_note
    @body_note = body_note 
    @d = Date.today 
  end  
  
def new_note
# Real applications authenticate with Evernote using OAuth, but for the
# purpose of exploring the API, you can get a developer token that allows
# you to access your own Evernote account. To get a developer token, visit
# https://sandbox.evernote.com/api/DeveloperToken.action
authToken = "S=s1:U=6d8b8:E=1469c221421:C=13f4470e824:P=1cd:A=en-devtoken:V=2:H=3daabff3d4a8ead1ddbb6b44f31c4a7e"

# if authToken == "f7:E=14635960d25:C=13edde4e127:P=1cd:A=en-devtoken:V=2:H=b8a91eab85aa90861db04c5f0a1e9d69"
#   puts "Please fill in your developer token"
#   puts "To get a developer token, visit https://sandbox.evernote.com/api/DeveloperToken.action"
#   exit(1)
# end

# Initial development is performed on our sandbox server. To use the production
# service, change "sandbox.evernote.com" to "www.evernote.com" and replace your
# developer token above with a token from
# https://www.evernote.com/api/DeveloperToken.action
evernoteHost = "sandbox.evernote.com"
userStoreUrl = "https://#{evernoteHost}/edam/user"

userStoreTransport = Thrift::HTTPClientTransport.new(userStoreUrl)
userStoreProtocol = Thrift::BinaryProtocol.new(userStoreTransport)
userStore = Evernote::EDAM::UserStore::UserStore::Client.new(userStoreProtocol)

versionOK = userStore.checkVersion("Evernote EDAMTest (Ruby)",
				   Evernote::EDAM::UserStore::EDAM_VERSION_MAJOR,
				   Evernote::EDAM::UserStore::EDAM_VERSION_MINOR)
puts "Is my Evernote API version up to date?  #{versionOK}"
puts
exit(1) unless versionOK

# Get the URL used to interact with the contents of the user's account
# When your application authenticates using OAuth, the NoteStore URL will
# be returned along with the auth token in the final OAuth request.
# In that case, you don't need to make this call.
noteStoreUrl = userStore.getNoteStoreUrl(authToken)

noteStoreTransport = Thrift::HTTPClientTransport.new(noteStoreUrl)
noteStoreProtocol = Thrift::BinaryProtocol.new(noteStoreTransport)
noteStore = Evernote::EDAM::NoteStore::NoteStore::Client.new(noteStoreProtocol)

# List all of the notebooks in the user's account
notebooks = noteStore.listNotebooks(authToken)
puts "Found #{notebooks.size} notebooks:"
defaultNotebook = notebooks.first
notebooks.each do |notebook|
  puts "  * #{notebook.name}"
end


puts
puts "Creating a new note in the default notebook: #{defaultNotebook.name}"
puts

# To create a new note, simply create a new Note object and fill in
# attributes such as the note's title.

note = Evernote::EDAM::Type::Note.new
note.title = @d.to_s + ' ' + @title_note
# To include an attachment such as an image in a note, first create a Resource
# for the attachment. At a minimum, the Resource contains the binary attachment
# data, an MD5 hash of the binary data, and the attachment MIME type. It can also
#/ include attributes such as filename and location.
filename = "/usr/pbx-ruby/jarvix/logo.png"
image = File.open(filename, "rb") { |io| io.read }
hashFunc = Digest::MD5.new

data = Evernote::EDAM::Type::Data.new
data.size = image.size
data.bodyHash = hashFunc.digest(image)
data.body = image

resource = Evernote::EDAM::Type::Resource.new
resource.mime = "image/png"
resource.data = data
resource.attributes = Evernote::EDAM::Type::ResourceAttributes.new
resource.attributes.fileName = filename

# Now, add the new Resource to the note's list of resources
note.resources = [ resource ]

# To display the Resource as part of the note's content, include an <en-media>
# tag in the note's ENML content. The en-media tag identifies the corresponding
# Resource using the MD5 hash.
hashHex = hashFunc.hexdigest(image)

# The content of an Evernote note is represented using Evernote Markup Language
# (ENML). The full ENML specification can be found in the Evernote API Overview
# at http://dev.evernote.com/documentation/cloud/chapters/ENML.php

note.content = <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE en-note SYSTEM "http://xml.evernote.com/pub/enml2.dtd">
<en-note>
	<table>
		<tr>
			<td><en-media width="60" height="60" type="image/png" hash="#{hashHex}"/></td> 
			<td> <span style="font-weight:bold;color:4FA22B;">#{@d.to_s} #{@title_note}</span> </td>
		</tr>
	</table>
    <br/>
    #{@body_note}<br/>
</en-note>
EOF

# Finally, send the new note to Evernote using the createNote method
# The new Note object that is returned will contain server-generated
# attributes such as the new note's unique GUID.
createdNote = noteStore.createNote(authToken, note)

puts "Successfully created a new note with GUID: #{createdNote.guid}"
  	
end

end 
