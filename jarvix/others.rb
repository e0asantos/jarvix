# autor :Seth Karim Luis Martinez
# email :guepardo190889@gmail.com
# date  :26/Mayo/2013
# file  :sendFBMessage.rb  
# description: this class make stuff into facebook with ruby 

require 'xmpp4r_facebook'
require 'fb_graph'
#require 'random'

class Stuff

	attr_accessor:user
	attr_accessor:userId
	attr_accessor:userTokenId
	attr_accessor:bodyMessage
	
	def initialize(userId, userTokenId, bodyMessage)  
    	@userId = userId
    	@userTokenId = userTokenId
    	@bodyMessage = bodyMessage

    	@user = FbGraph::User.me("#{userTokenId}")

    	puts "userId: #{@userId}"
    	puts "userTokenId: #{@userTokenId}"
    	puts "bodyMessage: #{@bodyMessage}"
    	puts "user.name: #{@user.name}"
  	end

	#Genera un numero aleatorio entre 1 y 100
  	def randomNum
  		puts "Entry to randomNum Method"
  		tmp = rand(1..100)
  		puts "rand: #{tmp}"
  		return tmp
  	end 

  	#def wallPost
  	#	@user.feed!(
  	#	:message => '#{@bodyMessage}',
  	#	:picture => 'http://media.tumblr.com/6cdd1cd0671665b1588644e9c9ab216a/tumblr_inline_mmrifmZWUB1qz4rgp.png',
  	#	:link => 'https://github.com/nov/fb_graph',
  	#	:name => 'Wall post',
  	#	:description => 'A Ruby wrapper for Facebook Graph API by #{@user.name}')
  	#end

  	#Genera un post en tu muro de Facebook
  	def wallPost
  		@user.feed!(
  			:message => "#{@bodyMessage} - #{randomNum}",
  			:picture => '',
  			:link => '',
  			:name => '',
  			:description => ''
  		)
  	end

  	#Crea una nota en tu Facebook
  	def postNote
		page = FbGraph::Page.new(117513961602338)
		note = page.note!(
		  :access_token => "#{@userTokenId}",
		  :subject => "Automatic Note from Aura",
		  :message => "#{@bodyMessage}"
		)  		
  	end  	
end
