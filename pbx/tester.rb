#!/usr/bin/env ruby
PROJECT_HOME = "/usr/pbx-ruby/"

require 'rubygems'
require 'AsteriskRuby'		#for Asterisk AGI
require 'net/http'		#for http connections
require 'yaml'
require 'active_record'
require '/usr/pbx-ruby/app/models/post.rb'
require 'net/http'
require 'uri'
require 'eventmachine'
require 'logger'
require 'json'
require '/usr/pbx-ruby/jarvix/Jarvix.rb'
require '/usr/pbx-ruby/jarvix/create_note.rb'
require '/usr/pbx-ruby/jarvix/sendFBMessage.rb'
require 'fb_graph'

message = SendFBMessage.new('100000079101495', "CAACEdEose0cBAJx1ffFNmSMOLTWmHSZBIqkHyOUvBZBZCgA9YBQrQWTr8jWgglVbkUmZAd5fR4NB30YXyD9yZBFe7T3eWs2oeHAb9KxjmlzhEuITANULUDiGIpQ4w0p38IGzuZCS1QKzZAmZCP3kTZA3pTieCDqGrvTgZD", 'Andres', "mensaje")
result = message.sendMessage