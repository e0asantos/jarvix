class Jarvix_Custom_Property < ActiveRecord::Base
	attr_accessible :id,:id_custom_object,:id_user,:is_enabled,:property_name,:property_value
	belongs_to :jarvix_custom_objects
	belongs_to :jarvix_personalities

	establish_connection(
  :adapter=> "mysql2",
  :encoding=> "utf8",
  :database=> "jarvix_interpreter",
  :pool=> "5",
  :username=> "root",
  :password=> "mysql123",
  :socket=> "/var/lib/mysql/mysql.sock"
)
end