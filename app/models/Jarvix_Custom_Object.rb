class Jarvix_Custom_Object < ActiveRecord::Base
	attr_accessible :id,:id_abstract_object,:id_owner,:custom_object_properties,:is_active
	belongs_to :jarvix_abstract_objects
	belongs_to :jarvix_personalities
	has_many :jarvix_custom_properties

	establish_connection(
  :adapter=> "mysql2",
  :encoding=> "utf8",
  :database=> "jarvix_interpreter",
  :pool=> "5",
  :username=> "root",
  :password=> "mysql123",
  :socket=> "/var/lib/mysql/mysql.sock"
)
end