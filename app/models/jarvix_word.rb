#!/usr/bin/env ruby
# encoding: utf-8
require 'rubygems'
require 'bing_translator'
class Jarvix_Word < ActiveRecord::Base
  attr_accessible :id,:word,:es_word,:word_type,:is_question,:is_sentence,:result_is,:is_breakline,:time_usage,:respect_level,:conversation_step

	def word=(word)
    if is_numeric(word)==false and validate_email_domain(word)==false
      translator = BingTranslator.new('lineaccessmx', 'k+Q3xRzEgOImBpDLGf/h1Xus3Rbby6WcEid4Khbsiv4=')
      translationincome=translator.translate word, :to => 'es'
      write_attribute(:es_word, translationincome)
      puts "<<<<<<Jarvix_Word.word<<<<<<<"+translationincome
    end
		write_attribute(:word, word)
  	
	end

	def word
    	return read_attribute(:word)  # No test for nil?
	end

	def translate
    translator = BingTranslator.new('lineaccessmx', 'k+Q3xRzEgOImBpDLGf/h1Xus3Rbby6WcEid4Khbsiv4=')
    translationincome=nil
    
    if read_attribute(:es_word)==nil and is_numeric(read_attribute(:word))==false and validate_email_domain(read_attribute(:word))==false
      translationincome=translator.translate read_attribute(:word), :to => 'es'
      puts "<<<<<<Jarvix_Word.translate<<<<<<<"
      write_attribute(:es_word, translationincome)
      save
    end
    if translationincome!=nil
      return translationincome
    else 
      return read_attribute(:es_word)
    end
  end

  establish_connection(
  :adapter=> "mysql2",
  :encoding=> "utf8",
  :database=> "jarvix_interpreter",
  :pool=> "5",
  :username=> "root",
  :password=> "mysql123",
  :socket=> "/var/lib/mysql/mysql.sock"
)
  def is_numeric(obj) 
      obj.to_s.match(/\A[+-]?\d+?(\.\d+)?\Z/) == nil ? false : true
  end

  def validate_email_domain(email)
      domain = email.match(/\@(.+)/)
      if domain!=nil
        domain=domain[1]
      else
        return false
      end
      Resolv::DNS.open do |dns|
          @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
      end
      @mx.size > 0 ? true : false
  end
end
